# Contributor: Nick Black <dankamongmen@gmail.com>
# Maintainer: Nick Black <dankamongmen@gmail.com>
pkgname=notcurses
pkgver=2.3.12
pkgrel=0
pkgdesc="Blingful character graphics and TUI library"
url="https://nick-black.com/dankwiki/index.php/Notcurses"
arch="all"
license="Apache-2.0"
makedepends="cmake doctest-dev ffmpeg-dev libunistring-dev linux-headers ncurses-dev ncurses-terminfo readline-dev zlib-dev"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev $pkgname-doc
	$pkgname-libs $pkgname-demo ncneofetch ncls $pkgname-view $pkgname-tetris"
source="https://github.com/dankamongmen/notcurses/archive/v$pkgver/notcurses-$pkgver.tar.gz
	https://github.com/dankamongmen/notcurses/releases/download/v$pkgver/notcurses-doc-$pkgver.tar.gz
	"

# Don't run tests on mips64, builder is too slow.
[ "$CARCH" = "mips64" ] && options="!check"
[ "$CARCH" = "riscv64" ] && options="textrels"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DUSE_PANDOC=off \
		-DUSE_QRCODEGEN=off \
		-DUSE_PYTHON=off \
		$CMAKE_CROSSOPTS
	make -C build
}

check() {
	env TERM=vt100 CTEST_OUTPUT_ON_FAILURE=1 make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
	for i in 1 3 ; do
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec echo "$pkgdir"/usr/share/man/man$i {} \;
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec install -Dm644 -t "$pkgdir"/usr/share/man/man$i {} \;
	done
}

libs() {
	amove usr/lib/libnotcurses*.so.*
}

demo() {
	amove usr/bin/notcurses-demo
	amove usr/bin/notcurses-tester
	amove usr/bin/notcurses-info
	amove usr/share/notcurses
}

ncneofetch() {
	amove usr/bin/ncneofetch
}

ncls() {
	amove usr/bin/ncls
}

view() {
	amove usr/bin/ncplayer
}

tetris() {
	amove usr/bin/nctetris
}

sha512sums="
83cbc91fcee871aa88eca3857930e9767a743c889aa8a85bf5bf26d46ba739adfd8edf67bf53b92383de58cfdc9a761d0854a7689fb30a0d5d1e2872c27e5c48  notcurses-2.3.12.tar.gz
c1636a2a4ee72dde60adcf0ebadf3c2265901ae99337ed3e91dfe915c518703f69bd302a85fd5733ca490abe89bf5fb65994945cdf27dd27a0d13d800800c4e6  notcurses-doc-2.3.12.tar.gz
"
