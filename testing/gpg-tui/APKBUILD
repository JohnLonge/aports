# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=gpg-tui
pkgver=0.7.3
pkgrel=0
pkgdesc="Terminal user interface for GnuPG"
url="https://github.com/orhun/gpg-tui"
license="MIT"
arch="all !s390x !mips64 !riscv64" # blocked by rust/cargo
makedepends="cargo gpgme-dev libxcb-dev libgpg-error-dev"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/orhun/gpg-tui/archive/v$pkgver/gpg-tui-$pkgver.tar.gz"

build() {
	cargo build --release --locked

	mkdir completions
	OUT_DIR=completions cargo run --release --bin completions
}

check() {
	cargo test --release --locked
}

package() {
	install -Dm755 target/release/$pkgname "$pkgdir"/usr/bin/$pkgname

	install -Dm644 man/$pkgname.1 "$pkgdir"/usr/share/man/man1/$pkgname.1

	install -Dm644 completions/$pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 completions/$pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 completions/_$pkgname "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
6eb11b227c4b54a5d3c126093cc76d924b892723032f26286876558a1755cdba0e8024a34809bbeaaadc2412e9d0b2025cabab9fdb9453be0517a64295cfa977  gpg-tui-0.7.3.tar.gz
"
